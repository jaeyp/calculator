# calculator

### Download Source code  

~~~
git clone https://bitbucket.org/jaeyp/calculator
~~~

### Open Source code  

~~~
open Calculator.sln in Visual Studio
~~~

### To build or rebuild a single project  

```
In Solution Explorer, choose or open the project.
On the menu bar, choose Build, and then choose either Build ProjectName or Rebuild ProjectName.
Choose Build ProjectName to build only those project components that have changed since the most recent build.
Choose Rebuild ProjectName to "clean" the project and then build the project files and all project components
```

### About LISENCE

I assign Apache License 2.0 to this project and usually prefer this license because there are relatively few ristrictions compared to GPL.
