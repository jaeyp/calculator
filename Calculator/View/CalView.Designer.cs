﻿namespace Calculator.View
{
    partial class CalView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CalView));
            this.buttonPlusMinus = new System.Windows.Forms.Button();
            this.buttonNum0 = new System.Windows.Forms.Button();
            this.buttonPoint = new System.Windows.Forms.Button();
            this.buttonCalculate = new System.Windows.Forms.Button();
            this.buttonAddition = new System.Windows.Forms.Button();
            this.buttonNum3 = new System.Windows.Forms.Button();
            this.buttonNum2 = new System.Windows.Forms.Button();
            this.buttonNum1 = new System.Windows.Forms.Button();
            this.buttonSubtraction = new System.Windows.Forms.Button();
            this.buttonNum6 = new System.Windows.Forms.Button();
            this.buttonNum5 = new System.Windows.Forms.Button();
            this.buttonNum4 = new System.Windows.Forms.Button();
            this.buttonMultipication = new System.Windows.Forms.Button();
            this.buttonNum9 = new System.Windows.Forms.Button();
            this.buttonNum8 = new System.Windows.Forms.Button();
            this.buttonNum7 = new System.Windows.Forms.Button();
            this.buttonDivision = new System.Windows.Forms.Button();
            this.buttonBackspace = new System.Windows.Forms.Button();
            this.buttonC = new System.Windows.Forms.Button();
            this.buttonCE = new System.Windows.Forms.Button();
            this.buttonOneDivideByX = new System.Windows.Forms.Button();
            this.buttonSecondPowerOfX = new System.Windows.Forms.Button();
            this.buttonSquareRoot = new System.Windows.Forms.Button();
            this.buttonPercent = new System.Windows.Forms.Button();
            this.labelScreen = new System.Windows.Forms.Label();
            this.buttonMemClear = new System.Windows.Forms.Button();
            this.buttonMemRead = new System.Windows.Forms.Button();
            this.buttonMemPlus = new System.Windows.Forms.Button();
            this.buttonMemMinus = new System.Windows.Forms.Button();
            this.buttonMemStore = new System.Windows.Forms.Button();
            this.labelSubScreen = new System.Windows.Forms.Label();
            this.buttonHistory = new System.Windows.Forms.Button();
            this.buttonNavBar = new System.Windows.Forms.Button();
            this.labelMode = new System.Windows.Forms.Label();
            this.labelHistory = new System.Windows.Forms.Label();
            this.buttonOnOff = new System.Windows.Forms.Button();
            this.panelMenu = new System.Windows.Forms.Panel();
            this.panelAbout = new System.Windows.Forms.Panel();
            this.linkLabelAbout = new System.Windows.Forms.LinkLabel();
            this.labelAboutContent1 = new System.Windows.Forms.Label();
            this.labelAbout = new System.Windows.Forms.Label();
            this.labelAboutContent2 = new System.Windows.Forms.Label();
            this.buttonProgrammer = new System.Windows.Forms.Button();
            this.buttonScientific = new System.Windows.Forms.Button();
            this.buttonAbout = new System.Windows.Forms.Button();
            this.labelHorizontalDivider = new System.Windows.Forms.Label();
            this.buttonStandard = new System.Windows.Forms.Button();
            this.labelMenu = new System.Windows.Forms.Label();
            this.panelMenu.SuspendLayout();
            this.panelAbout.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonPlusMinus
            // 
            this.buttonPlusMinus.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonPlusMinus.FlatAppearance.BorderSize = 0;
            this.buttonPlusMinus.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.buttonPlusMinus.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonPlusMinus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPlusMinus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.buttonPlusMinus.Image = ((System.Drawing.Image)(resources.GetObject("buttonPlusMinus.Image")));
            this.buttonPlusMinus.Location = new System.Drawing.Point(3, 469);
            this.buttonPlusMinus.Name = "buttonPlusMinus";
            this.buttonPlusMinus.Size = new System.Drawing.Size(64, 54);
            this.buttonPlusMinus.TabIndex = 0;
            this.buttonPlusMinus.UseVisualStyleBackColor = false;
            this.buttonPlusMinus.Click += new System.EventHandler(this.buttonPlusMinus_Click);
            // 
            // buttonNum0
            // 
            this.buttonNum0.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonNum0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNum0.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNum0.ForeColor = System.Drawing.Color.Black;
            this.buttonNum0.Location = new System.Drawing.Point(73, 469);
            this.buttonNum0.Name = "buttonNum0";
            this.buttonNum0.Size = new System.Drawing.Size(64, 54);
            this.buttonNum0.TabIndex = 1;
            this.buttonNum0.Text = "0";
            this.buttonNum0.UseVisualStyleBackColor = false;
            this.buttonNum0.Click += new System.EventHandler(this.buttonNum0_Click);
            // 
            // buttonPoint
            // 
            this.buttonPoint.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonPoint.FlatAppearance.BorderSize = 0;
            this.buttonPoint.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.buttonPoint.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonPoint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPoint.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.buttonPoint.Image = ((System.Drawing.Image)(resources.GetObject("buttonPoint.Image")));
            this.buttonPoint.Location = new System.Drawing.Point(142, 469);
            this.buttonPoint.Name = "buttonPoint";
            this.buttonPoint.Size = new System.Drawing.Size(64, 54);
            this.buttonPoint.TabIndex = 2;
            this.buttonPoint.UseVisualStyleBackColor = false;
            this.buttonPoint.Click += new System.EventHandler(this.buttonPoint_Click);
            // 
            // buttonCalculate
            // 
            this.buttonCalculate.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonCalculate.FlatAppearance.BorderSize = 0;
            this.buttonCalculate.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.buttonCalculate.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonCalculate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCalculate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.buttonCalculate.Image = ((System.Drawing.Image)(resources.GetObject("buttonCalculate.Image")));
            this.buttonCalculate.Location = new System.Drawing.Point(212, 469);
            this.buttonCalculate.Name = "buttonCalculate";
            this.buttonCalculate.Size = new System.Drawing.Size(64, 54);
            this.buttonCalculate.TabIndex = 3;
            this.buttonCalculate.UseVisualStyleBackColor = false;
            this.buttonCalculate.Click += new System.EventHandler(this.buttonCalculate_Click);
            // 
            // buttonAddition
            // 
            this.buttonAddition.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonAddition.FlatAppearance.BorderSize = 0;
            this.buttonAddition.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.buttonAddition.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonAddition.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddition.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.buttonAddition.Image = ((System.Drawing.Image)(resources.GetObject("buttonAddition.Image")));
            this.buttonAddition.Location = new System.Drawing.Point(212, 412);
            this.buttonAddition.Name = "buttonAddition";
            this.buttonAddition.Size = new System.Drawing.Size(64, 54);
            this.buttonAddition.TabIndex = 7;
            this.buttonAddition.UseVisualStyleBackColor = false;
            this.buttonAddition.Click += new System.EventHandler(this.buttonAddition_Click);
            // 
            // buttonNum3
            // 
            this.buttonNum3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonNum3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNum3.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNum3.ForeColor = System.Drawing.Color.Black;
            this.buttonNum3.Location = new System.Drawing.Point(142, 412);
            this.buttonNum3.Name = "buttonNum3";
            this.buttonNum3.Size = new System.Drawing.Size(64, 54);
            this.buttonNum3.TabIndex = 6;
            this.buttonNum3.Text = "3";
            this.buttonNum3.UseVisualStyleBackColor = false;
            this.buttonNum3.Click += new System.EventHandler(this.buttonNum3_Click);
            // 
            // buttonNum2
            // 
            this.buttonNum2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonNum2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNum2.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNum2.ForeColor = System.Drawing.Color.Black;
            this.buttonNum2.Location = new System.Drawing.Point(73, 412);
            this.buttonNum2.Name = "buttonNum2";
            this.buttonNum2.Size = new System.Drawing.Size(64, 54);
            this.buttonNum2.TabIndex = 5;
            this.buttonNum2.Text = "2";
            this.buttonNum2.UseVisualStyleBackColor = false;
            this.buttonNum2.Click += new System.EventHandler(this.buttonNum2_Click);
            // 
            // buttonNum1
            // 
            this.buttonNum1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonNum1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNum1.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNum1.ForeColor = System.Drawing.Color.Black;
            this.buttonNum1.Location = new System.Drawing.Point(3, 412);
            this.buttonNum1.Name = "buttonNum1";
            this.buttonNum1.Size = new System.Drawing.Size(64, 54);
            this.buttonNum1.TabIndex = 4;
            this.buttonNum1.Text = "1";
            this.buttonNum1.UseVisualStyleBackColor = false;
            this.buttonNum1.Click += new System.EventHandler(this.buttonNum1_Click);
            // 
            // buttonSubtraction
            // 
            this.buttonSubtraction.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonSubtraction.FlatAppearance.BorderSize = 0;
            this.buttonSubtraction.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.buttonSubtraction.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonSubtraction.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSubtraction.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.buttonSubtraction.Image = ((System.Drawing.Image)(resources.GetObject("buttonSubtraction.Image")));
            this.buttonSubtraction.Location = new System.Drawing.Point(212, 352);
            this.buttonSubtraction.Name = "buttonSubtraction";
            this.buttonSubtraction.Size = new System.Drawing.Size(64, 54);
            this.buttonSubtraction.TabIndex = 11;
            this.buttonSubtraction.UseVisualStyleBackColor = false;
            this.buttonSubtraction.Click += new System.EventHandler(this.buttonSubtraction_Click);
            // 
            // buttonNum6
            // 
            this.buttonNum6.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonNum6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNum6.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNum6.ForeColor = System.Drawing.Color.Black;
            this.buttonNum6.Location = new System.Drawing.Point(142, 352);
            this.buttonNum6.Name = "buttonNum6";
            this.buttonNum6.Size = new System.Drawing.Size(64, 54);
            this.buttonNum6.TabIndex = 10;
            this.buttonNum6.Text = "6";
            this.buttonNum6.UseVisualStyleBackColor = false;
            this.buttonNum6.Click += new System.EventHandler(this.buttonNum6_Click);
            // 
            // buttonNum5
            // 
            this.buttonNum5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonNum5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNum5.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNum5.ForeColor = System.Drawing.Color.Black;
            this.buttonNum5.Location = new System.Drawing.Point(73, 352);
            this.buttonNum5.Name = "buttonNum5";
            this.buttonNum5.Size = new System.Drawing.Size(64, 54);
            this.buttonNum5.TabIndex = 9;
            this.buttonNum5.Text = "5";
            this.buttonNum5.UseVisualStyleBackColor = false;
            this.buttonNum5.Click += new System.EventHandler(this.buttonNum5_Click);
            // 
            // buttonNum4
            // 
            this.buttonNum4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonNum4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNum4.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNum4.ForeColor = System.Drawing.Color.Black;
            this.buttonNum4.Location = new System.Drawing.Point(3, 352);
            this.buttonNum4.Name = "buttonNum4";
            this.buttonNum4.Size = new System.Drawing.Size(64, 54);
            this.buttonNum4.TabIndex = 8;
            this.buttonNum4.Text = "4";
            this.buttonNum4.UseVisualStyleBackColor = false;
            this.buttonNum4.Click += new System.EventHandler(this.buttonNum4_Click);
            // 
            // buttonMultipication
            // 
            this.buttonMultipication.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonMultipication.FlatAppearance.BorderSize = 0;
            this.buttonMultipication.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.buttonMultipication.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonMultipication.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMultipication.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.buttonMultipication.Image = ((System.Drawing.Image)(resources.GetObject("buttonMultipication.Image")));
            this.buttonMultipication.Location = new System.Drawing.Point(212, 295);
            this.buttonMultipication.Name = "buttonMultipication";
            this.buttonMultipication.Size = new System.Drawing.Size(64, 54);
            this.buttonMultipication.TabIndex = 15;
            this.buttonMultipication.UseVisualStyleBackColor = false;
            this.buttonMultipication.Click += new System.EventHandler(this.buttonMultipication_Click);
            // 
            // buttonNum9
            // 
            this.buttonNum9.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonNum9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNum9.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNum9.ForeColor = System.Drawing.Color.Black;
            this.buttonNum9.Location = new System.Drawing.Point(142, 295);
            this.buttonNum9.Name = "buttonNum9";
            this.buttonNum9.Size = new System.Drawing.Size(64, 54);
            this.buttonNum9.TabIndex = 14;
            this.buttonNum9.Text = "9";
            this.buttonNum9.UseVisualStyleBackColor = false;
            this.buttonNum9.Click += new System.EventHandler(this.buttonNum9_Click);
            // 
            // buttonNum8
            // 
            this.buttonNum8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonNum8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNum8.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNum8.ForeColor = System.Drawing.Color.Black;
            this.buttonNum8.Location = new System.Drawing.Point(73, 295);
            this.buttonNum8.Name = "buttonNum8";
            this.buttonNum8.Size = new System.Drawing.Size(64, 54);
            this.buttonNum8.TabIndex = 13;
            this.buttonNum8.Text = "8";
            this.buttonNum8.UseVisualStyleBackColor = false;
            this.buttonNum8.Click += new System.EventHandler(this.buttonNum8_Click);
            // 
            // buttonNum7
            // 
            this.buttonNum7.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonNum7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNum7.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNum7.ForeColor = System.Drawing.Color.Black;
            this.buttonNum7.Location = new System.Drawing.Point(3, 295);
            this.buttonNum7.Name = "buttonNum7";
            this.buttonNum7.Size = new System.Drawing.Size(64, 54);
            this.buttonNum7.TabIndex = 12;
            this.buttonNum7.Text = "7";
            this.buttonNum7.UseVisualStyleBackColor = false;
            this.buttonNum7.Click += new System.EventHandler(this.buttonNum7_Click);
            // 
            // buttonDivision
            // 
            this.buttonDivision.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonDivision.FlatAppearance.BorderSize = 0;
            this.buttonDivision.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.buttonDivision.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonDivision.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDivision.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.buttonDivision.Image = ((System.Drawing.Image)(resources.GetObject("buttonDivision.Image")));
            this.buttonDivision.Location = new System.Drawing.Point(212, 235);
            this.buttonDivision.Name = "buttonDivision";
            this.buttonDivision.Size = new System.Drawing.Size(64, 54);
            this.buttonDivision.TabIndex = 19;
            this.buttonDivision.UseVisualStyleBackColor = false;
            this.buttonDivision.Click += new System.EventHandler(this.buttonDivision_Click);
            // 
            // buttonBackspace
            // 
            this.buttonBackspace.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonBackspace.FlatAppearance.BorderSize = 0;
            this.buttonBackspace.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.buttonBackspace.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonBackspace.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBackspace.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.buttonBackspace.Image = ((System.Drawing.Image)(resources.GetObject("buttonBackspace.Image")));
            this.buttonBackspace.Location = new System.Drawing.Point(142, 235);
            this.buttonBackspace.Name = "buttonBackspace";
            this.buttonBackspace.Size = new System.Drawing.Size(64, 54);
            this.buttonBackspace.TabIndex = 18;
            this.buttonBackspace.UseVisualStyleBackColor = false;
            this.buttonBackspace.Click += new System.EventHandler(this.buttonBackspace_Click);
            // 
            // buttonC
            // 
            this.buttonC.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonC.FlatAppearance.BorderSize = 0;
            this.buttonC.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.buttonC.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonC.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.buttonC.Image = ((System.Drawing.Image)(resources.GetObject("buttonC.Image")));
            this.buttonC.Location = new System.Drawing.Point(73, 235);
            this.buttonC.Name = "buttonC";
            this.buttonC.Size = new System.Drawing.Size(64, 54);
            this.buttonC.TabIndex = 17;
            this.buttonC.UseVisualStyleBackColor = false;
            this.buttonC.Click += new System.EventHandler(this.buttonC_Click);
            // 
            // buttonCE
            // 
            this.buttonCE.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonCE.FlatAppearance.BorderSize = 0;
            this.buttonCE.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.buttonCE.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonCE.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCE.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.buttonCE.Image = ((System.Drawing.Image)(resources.GetObject("buttonCE.Image")));
            this.buttonCE.Location = new System.Drawing.Point(3, 235);
            this.buttonCE.Name = "buttonCE";
            this.buttonCE.Size = new System.Drawing.Size(64, 54);
            this.buttonCE.TabIndex = 16;
            this.buttonCE.UseVisualStyleBackColor = false;
            this.buttonCE.Click += new System.EventHandler(this.buttonCE_Click);
            // 
            // buttonOneDivideByX
            // 
            this.buttonOneDivideByX.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonOneDivideByX.FlatAppearance.BorderSize = 0;
            this.buttonOneDivideByX.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.buttonOneDivideByX.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonOneDivideByX.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOneDivideByX.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.buttonOneDivideByX.Image = ((System.Drawing.Image)(resources.GetObject("buttonOneDivideByX.Image")));
            this.buttonOneDivideByX.Location = new System.Drawing.Point(212, 178);
            this.buttonOneDivideByX.Name = "buttonOneDivideByX";
            this.buttonOneDivideByX.Size = new System.Drawing.Size(64, 54);
            this.buttonOneDivideByX.TabIndex = 23;
            this.buttonOneDivideByX.UseVisualStyleBackColor = false;
            // 
            // buttonSecondPowerOfX
            // 
            this.buttonSecondPowerOfX.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonSecondPowerOfX.FlatAppearance.BorderSize = 0;
            this.buttonSecondPowerOfX.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.buttonSecondPowerOfX.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonSecondPowerOfX.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSecondPowerOfX.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.buttonSecondPowerOfX.Image = ((System.Drawing.Image)(resources.GetObject("buttonSecondPowerOfX.Image")));
            this.buttonSecondPowerOfX.Location = new System.Drawing.Point(142, 178);
            this.buttonSecondPowerOfX.Name = "buttonSecondPowerOfX";
            this.buttonSecondPowerOfX.Size = new System.Drawing.Size(64, 54);
            this.buttonSecondPowerOfX.TabIndex = 22;
            this.buttonSecondPowerOfX.UseVisualStyleBackColor = false;
            // 
            // buttonSquareRoot
            // 
            this.buttonSquareRoot.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonSquareRoot.FlatAppearance.BorderSize = 0;
            this.buttonSquareRoot.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.buttonSquareRoot.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonSquareRoot.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSquareRoot.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.buttonSquareRoot.Image = ((System.Drawing.Image)(resources.GetObject("buttonSquareRoot.Image")));
            this.buttonSquareRoot.Location = new System.Drawing.Point(73, 178);
            this.buttonSquareRoot.Name = "buttonSquareRoot";
            this.buttonSquareRoot.Size = new System.Drawing.Size(64, 54);
            this.buttonSquareRoot.TabIndex = 21;
            this.buttonSquareRoot.UseVisualStyleBackColor = false;
            // 
            // buttonPercent
            // 
            this.buttonPercent.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonPercent.FlatAppearance.BorderSize = 0;
            this.buttonPercent.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.buttonPercent.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonPercent.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPercent.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.buttonPercent.Image = ((System.Drawing.Image)(resources.GetObject("buttonPercent.Image")));
            this.buttonPercent.Location = new System.Drawing.Point(3, 178);
            this.buttonPercent.Name = "buttonPercent";
            this.buttonPercent.Size = new System.Drawing.Size(64, 54);
            this.buttonPercent.TabIndex = 20;
            this.buttonPercent.UseVisualStyleBackColor = false;
            // 
            // labelScreen
            // 
            this.labelScreen.Font = new System.Drawing.Font("Arial", 30F, System.Drawing.FontStyle.Bold);
            this.labelScreen.ForeColor = System.Drawing.Color.Black;
            this.labelScreen.Location = new System.Drawing.Point(3, 82);
            this.labelScreen.Name = "labelScreen";
            this.labelScreen.Size = new System.Drawing.Size(273, 54);
            this.labelScreen.TabIndex = 24;
            this.labelScreen.Text = "0";
            this.labelScreen.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.labelScreen.UseMnemonic = false;
            // 
            // buttonMemClear
            // 
            this.buttonMemClear.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.buttonMemClear.Enabled = false;
            this.buttonMemClear.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonMemClear.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMemClear.Location = new System.Drawing.Point(3, 142);
            this.buttonMemClear.Name = "buttonMemClear";
            this.buttonMemClear.Size = new System.Drawing.Size(51, 30);
            this.buttonMemClear.TabIndex = 33;
            this.buttonMemClear.Text = "MC";
            this.buttonMemClear.UseVisualStyleBackColor = false;
            // 
            // buttonMemRead
            // 
            this.buttonMemRead.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.buttonMemRead.Enabled = false;
            this.buttonMemRead.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonMemRead.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMemRead.Location = new System.Drawing.Point(59, 142);
            this.buttonMemRead.Name = "buttonMemRead";
            this.buttonMemRead.Size = new System.Drawing.Size(51, 30);
            this.buttonMemRead.TabIndex = 34;
            this.buttonMemRead.Text = "MR";
            this.buttonMemRead.UseVisualStyleBackColor = false;
            // 
            // buttonMemPlus
            // 
            this.buttonMemPlus.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.buttonMemPlus.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonMemPlus.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMemPlus.Location = new System.Drawing.Point(115, 142);
            this.buttonMemPlus.Name = "buttonMemPlus";
            this.buttonMemPlus.Size = new System.Drawing.Size(51, 30);
            this.buttonMemPlus.TabIndex = 35;
            this.buttonMemPlus.Text = "M+";
            this.buttonMemPlus.UseVisualStyleBackColor = false;
            // 
            // buttonMemMinus
            // 
            this.buttonMemMinus.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.buttonMemMinus.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonMemMinus.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMemMinus.Location = new System.Drawing.Point(171, 142);
            this.buttonMemMinus.Name = "buttonMemMinus";
            this.buttonMemMinus.Size = new System.Drawing.Size(51, 30);
            this.buttonMemMinus.TabIndex = 36;
            this.buttonMemMinus.Text = "M-";
            this.buttonMemMinus.UseVisualStyleBackColor = false;
            // 
            // buttonMemStore
            // 
            this.buttonMemStore.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.buttonMemStore.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonMemStore.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMemStore.Location = new System.Drawing.Point(225, 142);
            this.buttonMemStore.Name = "buttonMemStore";
            this.buttonMemStore.Size = new System.Drawing.Size(51, 30);
            this.buttonMemStore.TabIndex = 37;
            this.buttonMemStore.Text = "MS";
            this.buttonMemStore.UseVisualStyleBackColor = false;
            // 
            // labelSubScreen
            // 
            this.labelSubScreen.Location = new System.Drawing.Point(3, 44);
            this.labelSubScreen.Name = "labelSubScreen";
            this.labelSubScreen.Size = new System.Drawing.Size(273, 33);
            this.labelSubScreen.TabIndex = 38;
            this.labelSubScreen.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // buttonHistory
            // 
            this.buttonHistory.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.buttonHistory.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonHistory.BackgroundImage")));
            this.buttonHistory.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonHistory.FlatAppearance.BorderSize = 0;
            this.buttonHistory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonHistory.Location = new System.Drawing.Point(236, 2);
            this.buttonHistory.Name = "buttonHistory";
            this.buttonHistory.Size = new System.Drawing.Size(39, 37);
            this.buttonHistory.TabIndex = 39;
            this.buttonHistory.UseVisualStyleBackColor = false;
            this.buttonHistory.Click += new System.EventHandler(this.buttonHistory_Click);
            // 
            // buttonNavBar
            // 
            this.buttonNavBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.buttonNavBar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonNavBar.BackgroundImage")));
            this.buttonNavBar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonNavBar.FlatAppearance.BorderSize = 0;
            this.buttonNavBar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNavBar.Location = new System.Drawing.Point(3, 2);
            this.buttonNavBar.Name = "buttonNavBar";
            this.buttonNavBar.Size = new System.Drawing.Size(39, 37);
            this.buttonNavBar.TabIndex = 40;
            this.buttonNavBar.UseVisualStyleBackColor = false;
            this.buttonNavBar.Click += new System.EventHandler(this.buttonNavBar_Click);
            // 
            // labelMode
            // 
            this.labelMode.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMode.Location = new System.Drawing.Point(45, 2);
            this.labelMode.Name = "labelMode";
            this.labelMode.Size = new System.Drawing.Size(144, 37);
            this.labelMode.TabIndex = 41;
            this.labelMode.Text = "Standard";
            this.labelMode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelHistory
            // 
            this.labelHistory.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHistory.Location = new System.Drawing.Point(1, 178);
            this.labelHistory.Name = "labelHistory";
            this.labelHistory.Size = new System.Drawing.Size(276, 348);
            this.labelHistory.TabIndex = 42;
            this.labelHistory.Text = "There\'s no history yet";
            this.labelHistory.Visible = false;
            // 
            // buttonOnOff
            // 
            this.buttonOnOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.buttonOnOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonOnOff.FlatAppearance.BorderSize = 0;
            this.buttonOnOff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOnOff.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOnOff.Location = new System.Drawing.Point(187, 2);
            this.buttonOnOff.Name = "buttonOnOff";
            this.buttonOnOff.Size = new System.Drawing.Size(47, 37);
            this.buttonOnOff.TabIndex = 43;
            this.buttonOnOff.Text = "ON";
            this.buttonOnOff.UseVisualStyleBackColor = false;
            this.buttonOnOff.Click += new System.EventHandler(this.buttonOnOff_Click);
            // 
            // panelMenu
            // 
            this.panelMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(248)))));
            this.panelMenu.Controls.Add(this.panelAbout);
            this.panelMenu.Controls.Add(this.buttonProgrammer);
            this.panelMenu.Controls.Add(this.buttonScientific);
            this.panelMenu.Controls.Add(this.buttonAbout);
            this.panelMenu.Controls.Add(this.labelHorizontalDivider);
            this.panelMenu.Controls.Add(this.buttonStandard);
            this.panelMenu.Controls.Add(this.labelMenu);
            this.panelMenu.Location = new System.Drawing.Point(3, 2);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Size = new System.Drawing.Size(206, 523);
            this.panelMenu.TabIndex = 44;
            this.panelMenu.Visible = false;
            this.panelMenu.Paint += new System.Windows.Forms.PaintEventHandler(this.panelMenu_Paint);
            // 
            // panelAbout
            // 
            this.panelAbout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(248)))));
            this.panelAbout.Controls.Add(this.linkLabelAbout);
            this.panelAbout.Controls.Add(this.labelAboutContent1);
            this.panelAbout.Controls.Add(this.labelAbout);
            this.panelAbout.Controls.Add(this.labelAboutContent2);
            this.panelAbout.Location = new System.Drawing.Point(0, 0);
            this.panelAbout.Name = "panelAbout";
            this.panelAbout.Size = new System.Drawing.Size(206, 523);
            this.panelAbout.TabIndex = 46;
            this.panelAbout.Visible = false;
            // 
            // linkLabelAbout
            // 
            this.linkLabelAbout.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabelAbout.Location = new System.Drawing.Point(12, 208);
            this.linkLabelAbout.Name = "linkLabelAbout";
            this.linkLabelAbout.Size = new System.Drawing.Size(184, 24);
            this.linkLabelAbout.TabIndex = 45;
            this.linkLabelAbout.TabStop = true;
            this.linkLabelAbout.Text = "Email Me";
            this.linkLabelAbout.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelAbout_LinkClicked);
            // 
            // labelAboutContent1
            // 
            this.labelAboutContent1.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAboutContent1.Location = new System.Drawing.Point(9, 56);
            this.labelAboutContent1.Name = "labelAboutContent1";
            this.labelAboutContent1.Size = new System.Drawing.Size(187, 31);
            this.labelAboutContent1.TabIndex = 43;
            this.labelAboutContent1.Text = "Calculator";
            this.labelAboutContent1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelAbout
            // 
            this.labelAbout.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAbout.Location = new System.Drawing.Point(44, 4);
            this.labelAbout.Name = "labelAbout";
            this.labelAbout.Size = new System.Drawing.Size(148, 29);
            this.labelAbout.TabIndex = 42;
            this.labelAbout.Text = "About";
            this.labelAbout.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelAboutContent2
            // 
            this.labelAboutContent2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAboutContent2.Location = new System.Drawing.Point(9, 114);
            this.labelAboutContent2.Name = "labelAboutContent2";
            this.labelAboutContent2.Size = new System.Drawing.Size(187, 67);
            this.labelAboutContent2.TabIndex = 44;
            this.labelAboutContent2.Text = "Version 0.11b\n© 2018 Jaehyun Park. All rights reserved.";
            // 
            // buttonProgrammer
            // 
            this.buttonProgrammer.Enabled = false;
            this.buttonProgrammer.FlatAppearance.BorderSize = 0;
            this.buttonProgrammer.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonProgrammer.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonProgrammer.Location = new System.Drawing.Point(1, 133);
            this.buttonProgrammer.Name = "buttonProgrammer";
            this.buttonProgrammer.Size = new System.Drawing.Size(203, 47);
            this.buttonProgrammer.TabIndex = 47;
            this.buttonProgrammer.Text = "          Programmer";
            this.buttonProgrammer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonProgrammer.UseVisualStyleBackColor = true;
            this.buttonProgrammer.Click += new System.EventHandler(this.buttonProgrammer_Click);
            // 
            // buttonScientific
            // 
            this.buttonScientific.Enabled = false;
            this.buttonScientific.FlatAppearance.BorderSize = 0;
            this.buttonScientific.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonScientific.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonScientific.Location = new System.Drawing.Point(1, 88);
            this.buttonScientific.Name = "buttonScientific";
            this.buttonScientific.Size = new System.Drawing.Size(203, 47);
            this.buttonScientific.TabIndex = 46;
            this.buttonScientific.Text = "          Scientific";
            this.buttonScientific.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonScientific.UseVisualStyleBackColor = true;
            this.buttonScientific.Click += new System.EventHandler(this.buttonScientific_Click);
            // 
            // buttonAbout
            // 
            this.buttonAbout.FlatAppearance.BorderSize = 0;
            this.buttonAbout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAbout.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAbout.Location = new System.Drawing.Point(1, 476);
            this.buttonAbout.Name = "buttonAbout";
            this.buttonAbout.Size = new System.Drawing.Size(203, 47);
            this.buttonAbout.TabIndex = 45;
            this.buttonAbout.Text = "          About";
            this.buttonAbout.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAbout.UseVisualStyleBackColor = true;
            this.buttonAbout.Click += new System.EventHandler(this.buttonAbout_Click);
            // 
            // labelHorizontalDivider
            // 
            this.labelHorizontalDivider.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelHorizontalDivider.Location = new System.Drawing.Point(-2, 471);
            this.labelHorizontalDivider.Name = "labelHorizontalDivider";
            this.labelHorizontalDivider.Size = new System.Drawing.Size(206, 2);
            this.labelHorizontalDivider.TabIndex = 44;
            this.labelHorizontalDivider.Click += new System.EventHandler(this.labelHorizontalDivider_Click);
            // 
            // buttonStandard
            // 
            this.buttonStandard.FlatAppearance.BorderSize = 0;
            this.buttonStandard.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonStandard.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonStandard.Location = new System.Drawing.Point(1, 42);
            this.buttonStandard.Name = "buttonStandard";
            this.buttonStandard.Size = new System.Drawing.Size(203, 47);
            this.buttonStandard.TabIndex = 43;
            this.buttonStandard.Text = "          Standard";
            this.buttonStandard.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonStandard.UseVisualStyleBackColor = true;
            this.buttonStandard.Click += new System.EventHandler(this.buttonStandard_Click);
            // 
            // labelMenu
            // 
            this.labelMenu.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMenu.Location = new System.Drawing.Point(44, 1);
            this.labelMenu.Name = "labelMenu";
            this.labelMenu.Size = new System.Drawing.Size(145, 37);
            this.labelMenu.TabIndex = 42;
            this.labelMenu.Text = "Calculator";
            this.labelMenu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelMenu.Click += new System.EventHandler(this.labelMenu_Click);
            // 
            // CalView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ClientSize = new System.Drawing.Size(279, 528);
            this.Controls.Add(this.buttonNavBar);
            this.Controls.Add(this.panelMenu);
            this.Controls.Add(this.buttonOnOff);
            this.Controls.Add(this.labelMode);
            this.Controls.Add(this.buttonHistory);
            this.Controls.Add(this.labelSubScreen);
            this.Controls.Add(this.buttonPercent);
            this.Controls.Add(this.buttonMemStore);
            this.Controls.Add(this.buttonMemMinus);
            this.Controls.Add(this.buttonMemPlus);
            this.Controls.Add(this.buttonMemRead);
            this.Controls.Add(this.buttonMemClear);
            this.Controls.Add(this.labelScreen);
            this.Controls.Add(this.buttonOneDivideByX);
            this.Controls.Add(this.buttonSecondPowerOfX);
            this.Controls.Add(this.buttonSquareRoot);
            this.Controls.Add(this.buttonC);
            this.Controls.Add(this.buttonCE);
            this.Controls.Add(this.buttonMultipication);
            this.Controls.Add(this.buttonNum9);
            this.Controls.Add(this.buttonNum8);
            this.Controls.Add(this.buttonNum7);
            this.Controls.Add(this.buttonSubtraction);
            this.Controls.Add(this.buttonNum6);
            this.Controls.Add(this.buttonNum5);
            this.Controls.Add(this.buttonNum4);
            this.Controls.Add(this.buttonAddition);
            this.Controls.Add(this.buttonNum3);
            this.Controls.Add(this.buttonNum2);
            this.Controls.Add(this.buttonNum1);
            this.Controls.Add(this.buttonCalculate);
            this.Controls.Add(this.buttonPoint);
            this.Controls.Add(this.buttonNum0);
            this.Controls.Add(this.buttonPlusMinus);
            this.Controls.Add(this.buttonBackspace);
            this.Controls.Add(this.buttonDivision);
            this.Controls.Add(this.labelHistory);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximumSize = new System.Drawing.Size(295, 567);
            this.Name = "CalView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Calculator";
            this.panelMenu.ResumeLayout(false);
            this.panelAbout.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonPlusMinus;
        private System.Windows.Forms.Button buttonNum0;
        private System.Windows.Forms.Button buttonPoint;
        private System.Windows.Forms.Button buttonCalculate;
        private System.Windows.Forms.Button buttonAddition;
        private System.Windows.Forms.Button buttonNum3;
        private System.Windows.Forms.Button buttonNum2;
        private System.Windows.Forms.Button buttonNum1;
        private System.Windows.Forms.Button buttonSubtraction;
        private System.Windows.Forms.Button buttonNum6;
        private System.Windows.Forms.Button buttonNum5;
        private System.Windows.Forms.Button buttonNum4;
        private System.Windows.Forms.Button buttonMultipication;
        private System.Windows.Forms.Button buttonNum9;
        private System.Windows.Forms.Button buttonNum8;
        private System.Windows.Forms.Button buttonNum7;
        private System.Windows.Forms.Button buttonDivision;
        private System.Windows.Forms.Button buttonBackspace;
        private System.Windows.Forms.Button buttonC;
        private System.Windows.Forms.Button buttonCE;
        private System.Windows.Forms.Button buttonOneDivideByX;
        private System.Windows.Forms.Button buttonSecondPowerOfX;
        private System.Windows.Forms.Button buttonSquareRoot;
        private System.Windows.Forms.Button buttonPercent;
        private System.Windows.Forms.Label labelScreen;
        private System.Windows.Forms.Button buttonMemClear;
        private System.Windows.Forms.Button buttonMemRead;
        private System.Windows.Forms.Button buttonMemPlus;
        private System.Windows.Forms.Button buttonMemMinus;
        private System.Windows.Forms.Button buttonMemStore;
        private System.Windows.Forms.Label labelSubScreen;
        private System.Windows.Forms.Button buttonHistory;
        private System.Windows.Forms.Button buttonNavBar;
        private System.Windows.Forms.Label labelMode;
        private System.Windows.Forms.Label labelHistory;
        private System.Windows.Forms.Button buttonOnOff;
        private System.Windows.Forms.Panel panelMenu;
        private System.Windows.Forms.Button buttonStandard;
        private System.Windows.Forms.Label labelMenu;
        private System.Windows.Forms.Label labelHorizontalDivider;
        private System.Windows.Forms.Button buttonAbout;
        private System.Windows.Forms.Panel panelAbout;
        private System.Windows.Forms.Label labelAboutContent2;
        private System.Windows.Forms.Label labelAboutContent1;
        private System.Windows.Forms.Label labelAbout;
        private System.Windows.Forms.LinkLabel linkLabelAbout;
        private System.Windows.Forms.Button buttonProgrammer;
        private System.Windows.Forms.Button buttonScientific;
    }
}

