﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Text.RegularExpressions;
using Calculator.Controller;
using Calculator.Model;

namespace Calculator.View
{
    enum ViewEvent
    {
        None = 0,
        MenuOn = 101,
        MenuOff,
        AboutOn,
        AboutOff,
    }

    // panelAbout   240, 483
    // panelMenu    240, 483

    public partial class CalView : Form, ICalView
    {
        CalController _controller;

        Timer menuTimer;
        ViewEvent viewEvent;

        readonly int PANEL_MENU_WIDTH = 240;
        readonly int PANEL_MENU_mWIDTH = 30;

        public CalView()
        {
            InitializeComponent();

            menuTimer = new Timer();
            menuTimer.Tick += new EventHandler(_menu_tick); // Everytime timer ticks, _menu_tick will be called
            menuTimer.Interval = (10) * (1);                // Timer will tick every 10 millisecond

            viewEvent = ViewEvent.None;
        }

        void _menu_tick(object sender, EventArgs e)
        {
            switch(this.viewEvent)
            {
                case ViewEvent.MenuOn:
                    if (this.panelMenu.Width < PANEL_MENU_WIDTH)
                    {
                        this.panelMenu.Width += PANEL_MENU_mWIDTH;
                        this.menuTimer.Enabled = true;
                    }
                    else
                    {
                        this.menuTimer.Enabled = false;
                    }
                    break;
                case ViewEvent.MenuOff:
                    if (this.panelMenu.Width - PANEL_MENU_mWIDTH >= 0)
                    {
                        this.panelMenu.Width -= PANEL_MENU_mWIDTH;
                        this.menuTimer.Enabled = true;
                    }
                    else
                    {
                        this.menuTimer.Enabled = false;
                        this.panelMenu.Visible = false;
                    }
                    break;
                default:
                    break;
            }
        }

        #region ICalView implementation

        public void SetController(CalController controller)
        {
            _controller = controller;
        }

        public void ClearScreen(bool clearSubScreen = false)
        {
            if(clearSubScreen)
            {
                this.labelSubScreen.Text = "";
            }
            this.labelScreen.Font = new Font("Arial", 27.75f, FontStyle.Bold);
            this.labelScreen.TextAlign = ContentAlignment.BottomRight;
            this.labelScreen.Text = "0";
        }
        public void ClearSubScreen()
        {
            this.labelSubScreen.Text = "";
            // TODO: clear last token of labelSubScreen.Text
        }

        /*
        public void UpdateScreen(float emSize, double d)
        {
            this.labelScreen.Font = new Font("Arial", emSize, FontStyle.Bold);
            this.labelScreen.TextAlign = ContentAlignment.BottomRight;
            this.labelScreen.Text = String.Format("{0:#,##0.##############}", d);
            //this.labelScreen.Text = String.Format("{0:N}", d);
            //this.labelScreen.Text = String.Format("{0:E}", d);

            //labelScreen.Invalidate();
            //labelScreen.Update();
            //labelScreen.Refresh();
            //labelScreen.PerformLayout();
        }
        */

        public void UpdateScreen(double d, bool clearSubScreen = false)
        {
            float emSize = 0f;

            if (clearSubScreen)
            {
                this.labelSubScreen.Text = "";
            }

            MeasureScreenFontSize(d.ToString(), ref emSize);

            this.labelScreen.Font = new Font("Arial", emSize, FontStyle.Bold);
            this.labelScreen.TextAlign = ContentAlignment.BottomRight;
            this.labelScreen.Text = String.Format("{0:#,##0.##############}", d);
        }

        public void UpdateScreen(string s, bool clearSubScreen = false)
        {
            float emSize = 0f;

            if (clearSubScreen)
            {
                this.labelSubScreen.Text = "";
            }

            MeasureScreenFontSize(s, ref emSize);

            this.labelScreen.Font = new Font("Arial", emSize, FontStyle.Bold);
            this.labelScreen.TextAlign = ContentAlignment.BottomRight;
            this.labelScreen.Text = s;
        }

        /*
        public void UpdateScreenFont(string familyName, float emSize)
        {
            this.labelScreen.Font = new Font(familyName, emSize, FontStyle.Bold);
        }
        */

        public void SetPower(bool b)
        {
            if (b)
            {
                this.buttonOnOff.Text = "ON";
                this.labelScreen.Text = "0";
                //this.labelScreen.BackColor = SystemColors.ButtonFace;
                this.buttonNum0.BackColor = SystemColors.ControlLightLight;
                this.buttonNum1.BackColor = SystemColors.ControlLightLight;
                this.buttonNum2.BackColor = SystemColors.ControlLightLight;
                this.buttonNum3.BackColor = SystemColors.ControlLightLight;
                this.buttonNum4.BackColor = SystemColors.ControlLightLight;
                this.buttonNum5.BackColor = SystemColors.ControlLightLight;
                this.buttonNum6.BackColor = SystemColors.ControlLightLight;
                this.buttonNum7.BackColor = SystemColors.ControlLightLight;
                this.buttonNum8.BackColor = SystemColors.ControlLightLight;
                this.buttonNum9.BackColor = SystemColors.ControlLightLight;

                this.buttonNum0.FlatAppearance.BorderSize = 1;
                this.buttonNum1.FlatAppearance.BorderSize = 1;
                this.buttonNum2.FlatAppearance.BorderSize = 1;
                this.buttonNum3.FlatAppearance.BorderSize = 1;
                this.buttonNum4.FlatAppearance.BorderSize = 1;
                this.buttonNum5.FlatAppearance.BorderSize = 1;
                this.buttonNum6.FlatAppearance.BorderSize = 1;
                this.buttonNum7.FlatAppearance.BorderSize = 1;
                this.buttonNum8.FlatAppearance.BorderSize = 1;
                this.buttonNum9.FlatAppearance.BorderSize = 1;
            }
            else
            {
                this.buttonOnOff.Text = "OFF";
                this.labelScreen.Text = "";
                this.labelSubScreen.Text = "";
                //this.labelScreen.BackColor = Color.Black;
                this.buttonNum0.BackColor = SystemColors.ButtonShadow;
                this.buttonNum1.BackColor = SystemColors.ButtonShadow;
                this.buttonNum2.BackColor = SystemColors.ButtonShadow;
                this.buttonNum3.BackColor = SystemColors.ButtonShadow;
                this.buttonNum4.BackColor = SystemColors.ButtonShadow;
                this.buttonNum5.BackColor = SystemColors.ButtonShadow;
                this.buttonNum6.BackColor = SystemColors.ButtonShadow;
                this.buttonNum7.BackColor = SystemColors.ButtonShadow;
                this.buttonNum8.BackColor = SystemColors.ButtonShadow;
                this.buttonNum9.BackColor = SystemColors.ButtonShadow;

                this.buttonNum0.FlatAppearance.BorderSize = 0;
                this.buttonNum1.FlatAppearance.BorderSize = 0;
                this.buttonNum2.FlatAppearance.BorderSize = 0;
                this.buttonNum3.FlatAppearance.BorderSize = 0;
                this.buttonNum4.FlatAppearance.BorderSize = 0;
                this.buttonNum5.FlatAppearance.BorderSize = 0;
                this.buttonNum6.FlatAppearance.BorderSize = 0;
                this.buttonNum7.FlatAppearance.BorderSize = 0;
                this.buttonNum8.FlatAppearance.BorderSize = 0;
                this.buttonNum9.FlatAppearance.BorderSize = 0;
            }

            this.buttonHistory.Enabled = b;

            this.buttonPlusMinus.Enabled = b;
            this.buttonPoint.Enabled = b;
            this.buttonCalculate.Enabled = b;
            this.buttonNum0.Enabled = b;
            this.buttonNum1.Enabled = b;
            this.buttonNum2.Enabled = b;
            this.buttonNum3.Enabled = b;
            this.buttonNum4.Enabled = b;
            this.buttonNum5.Enabled = b;
            this.buttonNum6.Enabled = b;
            this.buttonNum7.Enabled = b;
            this.buttonNum8.Enabled = b;
            this.buttonNum9.Enabled = b;
            this.buttonAddition.Enabled = b;
            this.buttonSubtraction.Enabled = b;
            this.buttonMultipication.Enabled = b;
            this.buttonDivision.Enabled = b;
            this.buttonBackspace.Enabled = b;
            this.buttonC.Enabled = b;
            this.buttonCE.Enabled = b;
            this.buttonPercent.Enabled = b;
            this.buttonSquareRoot.Enabled = b;
            this.buttonSecondPowerOfX.Enabled = b;
            this.buttonOneDivideByX.Enabled = b;

            this.buttonMemClear.Enabled = false;
            this.buttonMemRead.Enabled = false;
            this.buttonMemMinus.Enabled = b;
            this.buttonMemPlus.Enabled = b;
            this.buttonMemStore.Enabled = b;
        }

        public void ShowMenu(bool b)
        {
            if(b)
            {
                this.viewEvent = ViewEvent.MenuOn;
                this.panelMenu.Width = 0;
                this.panelMenu.Visible = b;

                this.menuTimer.Enabled = true;
                //this.menuOnTimer.Enabled = true;
            }
            else
            {
                this.viewEvent = ViewEvent.MenuOff;
                this.panelMenu.Width = PANEL_MENU_WIDTH;

                this.menuTimer.Enabled = true;
                //this.menuOffTimer.Enabled = true;
            }
            //timer.Enabled = true;                       // Enable the timer
            //timer.Start();                              // Start the timer
        }

        public void ShowAbout(bool b)
        {
            if(b)
            {
                this.viewEvent = ViewEvent.AboutOn;
            }
            else
            {
                this.viewEvent = ViewEvent.AboutOff;
            }
            this.panelAbout.Visible = b;
        }

        public void ShowCalculator(bool b)
        {
            this.buttonPlusMinus.Visible = b;
            this.buttonPoint.Visible = b;
            this.buttonCalculate.Visible = b;
            this.buttonNum0.Visible = b;
            this.buttonNum1.Visible = b;
            this.buttonNum2.Visible = b;
            this.buttonNum3.Visible = b;
            this.buttonNum4.Visible = b;
            this.buttonNum5.Visible = b;
            this.buttonNum6.Visible = b;
            this.buttonNum7.Visible = b;
            this.buttonNum8.Visible = b;
            this.buttonNum9.Visible = b;
            this.buttonAddition.Visible = b;
            this.buttonSubtraction.Visible = b;
            this.buttonMultipication.Visible = b;
            this.buttonDivision.Visible = b;
            this.buttonBackspace.Visible = b;
            this.buttonC.Visible = b;
            this.buttonCE.Visible = b;
            this.buttonPercent.Visible = b;
            this.buttonSquareRoot.Visible = b;
            this.buttonSecondPowerOfX.Visible = b;
            this.buttonOneDivideByX.Visible = b;
        }

        public void ShowHistory(bool b)
        {
            this.labelHistory.Visible = b;
        }

        public void MeasureScreenFontSize(string s, ref float emSize)
        {
            // Make a Graphics object to measure the text.
            using (Graphics gr = this.labelScreen.CreateGraphics())
            {
                float minSize = 1;
                float maxSize = this.labelScreen.Size.Width;
                int width = this.labelScreen.Size.Width - (this.labelScreen.Size.Width / 10);
                int height = this.labelScreen.Size.Height;

                while (maxSize - minSize > 1f)
                {
                    float pt = (minSize + maxSize) / 2f;
                    using (Font testFont =
                        new Font(this.labelScreen.Font.FontFamily, pt))
                    {
                        // See if this font is too big.
                        SizeF textSize =
                            gr.MeasureString(s, testFont);

                        //labelSubScreen.Text = textSize.Width.ToString(); // debug

                        if ((textSize.Width * 1.1 > width) ||
                            (textSize.Height > height))
                            maxSize = pt;
                        else
                            minSize = pt;
                    }
                }
                emSize = minSize;
                //labelSubScreen.Text = emSize.ToString(); // debug
            }
        }

        public string ScreenText
        {
            get { return this.labelScreen.Text; }
            set { this.labelScreen.Text = value; }
        }

        public string SubScreenText
        {
            get { return this.labelSubScreen.Text; }
            set { this.labelSubScreen.Text = value; }
        }

        public string HistoryText
        {
            get { return this.labelHistory.Text; }
            set { this.labelHistory.Text = value; }
        }

        #endregion

        private void buttonNum1_Click(object sender, EventArgs e)
        {
            this._controller.ProcessInput(CalController.KeyCode.Num1);
            //this._controller.OnDigit(1);
        }

        private void buttonNum2_Click(object sender, EventArgs e)
        {
            this._controller.ProcessInput(CalController.KeyCode.Num2);
            //this._controller.OnDigit(2);
        }

        private void buttonNum3_Click(object sender, EventArgs e)
        {
            this._controller.ProcessInput(CalController.KeyCode.Num3);
            //this._controller.OnDigit(3);
        }

        private void buttonNum4_Click(object sender, EventArgs e)
        {
            this._controller.ProcessInput(CalController.KeyCode.Num4);
            //this._controller.OnDigit(4);
        }

        private void buttonNum5_Click(object sender, EventArgs e)
        {
            this._controller.ProcessInput(CalController.KeyCode.Num5);
            //this._controller.OnDigit(5);
        }

        private void buttonNum6_Click(object sender, EventArgs e)
        {
            this._controller.ProcessInput(CalController.KeyCode.Num6);
            //this._controller.OnDigit(6);
        }

        private void buttonNum7_Click(object sender, EventArgs e)
        {
            this._controller.ProcessInput(CalController.KeyCode.Num7);
            //this._controller.OnDigit(7);
        }

        private void buttonNum8_Click(object sender, EventArgs e)
        {
            this._controller.ProcessInput(CalController.KeyCode.Num8);
            //this._controller.OnDigit(8);
        }

        private void buttonNum9_Click(object sender, EventArgs e)
        {
            this._controller.ProcessInput(CalController.KeyCode.Num9);
            //this._controller.OnDigit(9);
        }

        private void buttonNum0_Click(object sender, EventArgs e)
        {
            this._controller.ProcessInput(CalController.KeyCode.Num0);
            //this._controller.OnDigit(0);
        }

        private void buttonPlusMinus_Click(object sender, EventArgs e)
        {
            this._controller.ProcessInput(CalController.KeyCode.PlusMinus);
        }

        private void buttonPoint_Click(object sender, EventArgs e)
        {
            this._controller.ProcessInput(CalController.KeyCode.Point);
        }

        private void buttonCE_Click(object sender, EventArgs e)
        {
            this._controller.ProcessInput(CalController.KeyCode.ClearError);
            //this._controller.OnEscape();
        }

        private void buttonC_Click(object sender, EventArgs e)
        {
            this._controller.ProcessInput(CalController.KeyCode.Clear);
            //this._controller.OnDelete();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            // in case that key didn't catched before.
            if (!base.ProcessCmdKey(ref msg, keyData))
            {
                // Debug: this.labelSubScreen.Text = keyData.ToString();
                switch (keyData)
                {
                    case Keys.D1:
                        this._controller.ProcessInput(CalController.KeyCode.Num1);
                        break;
                    case Keys.D2:
                        this._controller.ProcessInput(CalController.KeyCode.Num2);
                        break;
                    case Keys.D3:
                        this._controller.ProcessInput(CalController.KeyCode.Num3);
                        break;
                    case Keys.D4:
                        this._controller.ProcessInput(CalController.KeyCode.Num4);
                        break;
                    case Keys.D5:
                        this._controller.ProcessInput(CalController.KeyCode.Num5);
                        break;
                    case Keys.D6:
                        this._controller.ProcessInput(CalController.KeyCode.Num6);
                        break;
                    case Keys.D7:
                        this._controller.ProcessInput(CalController.KeyCode.Num7);
                        break;
                    case Keys.D8:
                        this._controller.ProcessInput(CalController.KeyCode.Num8);
                        break;
                    case Keys.D9:
                        this._controller.ProcessInput(CalController.KeyCode.Num9);
                        break;
                    case Keys.D0:
                        this._controller.ProcessInput(CalController.KeyCode.Num0);
                        break;
                    case Keys.Delete:
                        this._controller.ProcessInput(CalController.KeyCode.Clear);
                        //this._controller.OnDelete();
                        break;
                    case Keys.Escape:
                        this._controller.ProcessInput(CalController.KeyCode.ClearError);
                        //this._controller.OnEscape();
                        break;
                    case Keys.Back:
                        this._controller.ProcessInput(CalController.KeyCode.BackSpace);
                        //this._controller.OnBackspace();
                        break;
                    case Keys.Add:
                    //case Keys.Oemplus:
                        this._controller.ProcessInput(CalController.KeyCode.Add);
                        break;
                    case Keys.Subtract:
                    //case Keys.OemMinus:
                        this._controller.ProcessInput(CalController.KeyCode.Subtract);
                        break;
                    case Keys.Multiply:
                        this._controller.ProcessInput(CalController.KeyCode.Multiply);
                        break;
                    case Keys.Divide:
                        this._controller.ProcessInput(CalController.KeyCode.Divide);
                        break;
                    case Keys.Enter:
                        this._controller.ProcessInput(CalController.KeyCode.Calculate);
                        break;
                    default:
                        break;
                }
            }
            return true;
        }

        private void buttonBackspace_Click(object sender, EventArgs e)
        {
            this._controller.ProcessInput(CalController.KeyCode.BackSpace);
            //this._controller.OnBackspace();
        }

        private void buttonHistory_Click(object sender, EventArgs e)
        {
            this._controller.OnHistory();
        }

        private void buttonAddition_Click(object sender, EventArgs e)
        {
            this._controller.ProcessInput(CalController.KeyCode.Add);
            /*
            string s = this.labelSubScreen.Text;
            //calculate s
            s = s.Replace(" ","");
            char[] delimiter = { '+', '-', '*', '/' };
            string[] tokens = s.Split(delimiter);

            if(tokens.Length == 1)
            {
                s += " + ";
                this.labelSubScreen.Text = s;
            }
            else
            {
                this.labelSubScreen.Text = "";
                int i = 0;
                foreach (string t in tokens)
                {
                    this.labelSubScreen.Text += "[" + i + "]" + t;
                }
            }
            */
        }

        private void buttonSubtraction_Click(object sender, EventArgs e)
        {
            this._controller.ProcessInput(CalController.KeyCode.Subtract);
        }

        private void buttonMultipication_Click(object sender, EventArgs e)
        {
            this._controller.ProcessInput(CalController.KeyCode.Multiply);
        }

        private void buttonDivision_Click(object sender, EventArgs e)
        {
            this._controller.ProcessInput(CalController.KeyCode.Divide);
        }

        private void buttonCalculate_Click(object sender, EventArgs e)
        {
            this._controller.ProcessInput(CalController.KeyCode.Calculate);
        }

        private void buttonOnOff_Click(object sender, EventArgs e)
        {
            this._controller.OnPower();
        }

        private void buttonNavBar_Click(object sender, EventArgs e)
        {
            this._controller.OnNavBar();
        }

        private void buttonStandard_Click(object sender, EventArgs e)
        {
            this._controller.OnNavBar();
            //this._Controller.SetModeStandard();
        }

        private void buttonAbout_Click(object sender, EventArgs e)
        {
            this._controller.OnAbout();
        }

        private void labelMenu_Click(object sender, EventArgs e)
        {

        }

        private void panelMenu_Paint(object sender, PaintEventArgs e)
        {

        }

        private void labelHorizontalDivider_Click(object sender, EventArgs e)
        {

        }

        private void linkLabelAbout_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            linkLabelAbout.LinkVisited = true;
            System.Diagnostics.Process.Start("mailto:jp.inseoul@gmail.com");
        }

        private void buttonScientific_Click(object sender, EventArgs e)
        {

        }

        private void buttonProgrammer_Click(object sender, EventArgs e)
        {

        }
    }
}
