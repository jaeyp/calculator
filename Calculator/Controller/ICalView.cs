﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;
using Calculator.Model;

namespace Calculator.Controller
{
    public interface ICalView
    {
        void SetController(CalController controller);
        void SetPower(bool b);
        void ShowMenu(bool b);
        void ShowAbout(bool b);
        void ShowCalculator(bool b);
        void ShowHistory(bool b);
        void UpdateScreen(double d, bool clearSubScreen = false);
        void UpdateScreen(string s, bool clearSubScreen = false);
        void ClearScreen(bool clearSubScreen=false);
        void ClearSubScreen();

        //void MeasureScreenFontSize(string s, ref float emSize);

        string ScreenText { get; set; }
        string SubScreenText { get; set; }
        string HistoryText { get; set; }
    }
}
