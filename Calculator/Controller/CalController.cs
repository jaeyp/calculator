﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Text.RegularExpressions;
using System.Threading;
using Calculator.Model;

namespace Calculator.Controller
{
    public class CalController
    {
        enum InputState
        {
            Init = 0,
            OperandIntegerPart,
            OperandFractionalPart,
            Operator,
        }

        [Flags] public enum KeyCode
        {
            None = 0,
            OperandMask = 0x1000,
            Num0 = 0x1000,
            Num1 = 0x1001,
            Num2 = 0x1002,
            Num3 = 0x1003,
            Num4 = 0x1004,
            Num5 = 0x1005,
            Num6 = 0x1006,
            Num7 = 0x1007,
            Num8 = 0x1008,
            Num9 = 0x1009,

            OperatorMask = 0x2000,
            Calculate = 0x2000,
            Add = 0x2003,
            Subtract = 0x2004,
            Multiply = 0x2005,
            Divide = 0x2006,
            Percent = 0x2007,
            SquareRoot = 0x2008,
            SecondPowerOfX = 0x2009,
            OneDivideByX = 0x2010,

            PlusMinus = 0x2100,
            Point = 0x2101,

            ClearError = 0x2200,
            Clear = 0x2201,
            BackSpace = 0x2202,

            MemoryClear = 0x2300,
            MemoryRead = 0x2301,
            MemoryPlus = 0x2302,
            MemoryMinus = 0x2303,
            MemorySave = 0x2304,
        }

        private bool IsOperand(KeyCode input)
        {
            return (input & KeyCode.OperandMask) == KeyCode.OperandMask;
        }

        private bool IsOperator(KeyCode input)
        {
            return (input & KeyCode.OperatorMask) == KeyCode.OperatorMask;
        }

        private bool IsPoint(KeyCode input)
        {
            return (input == KeyCode.Point) ? true : false;
        }

        private ICalView _view;
        private CalModel _cal;
        private static InputState inputState = InputState.Init;
        private static bool isNegative = false;

        public CalController(ICalView view)
        {
            _cal = new CalModel();
            _view = view;
        }

        public void Initialize()
        {
            _view.SetController(this);
            _view.SetPower(_cal.PowerOn);
        }

        /*
         * State Machine for processing key input.
         */
        public void ProcessInput(KeyCode input)
        {
            if (!_cal.PowerOn)
                return;
            /*
             * PRE-STATE MACHINE
             */
            switch(input)
            {
                case KeyCode.Clear:
                    _view.ClearScreen(clearSubScreen: true);
                    return;
                case KeyCode.ClearError:
                    _view.ClearScreen(clearSubScreen: false);
                    return;
                case KeyCode.BackSpace:
                    DoBackSpace();
                    return;
                case KeyCode.PlusMinus:
                    TogglePlusMinus();
                    return;
                default:
                    break;
            }

            /*
             * STATE MACHINE
             * 
             *  State:Init
             *  State:OperandIntegerPart
             *  State:OperandFractionalPart
             *  State:Operator
             */
            switch (inputState)
            {
                case InputState.Init:
                    /* Only digit input is valid
                     * update main screen
                     * goto OperandIntPart state
                     */
                    if (IsPoint(input))
                    {
                        if(!_view.ScreenText.Contains('.'))
                        {
                            _view.ScreenText += ".";
                        }
                        inputState = InputState.OperandFractionalPart;
                    }
                    else if (IsOperand(input))
                    {
                        AddDigitOnScreen(input, true);
                        inputState = InputState.OperandIntegerPart;
                    }
                    else if (IsOperator(input))
                    {
                        string s = "";

                        if (!ConvertKeyCodeToString(input, ref s))
                        {
                            return;
                        }
                        // 1. Find last operand & operator token from SubScreenText
                        // 2. calculate last operand
                        _view.SubScreenText = _view.ScreenText + s;
                        inputState = InputState.Operator;
                    }
                    break;
                case InputState.OperandIntegerPart:
                    /* 1. point input
                     * s += input
                     * goto OperandDecimalPart state
                     * 2. digit input
                     * s += input
                     * update main screen 
                     * 3. operator input
                     * if sub string has perfect formula, 
                     * then calculate & update both screen (main & sub)
                     * else
                     * update sub screen += " operator "
                     * goto Operator state
                     */
                    if( IsPoint(input) )
                    {
                        _view.ScreenText += ".";
                        inputState = InputState.OperandFractionalPart;
                    }
                    else if( IsOperand(input) )
                    {
                        AddDigitOnScreen(input);
                    }
                    else if( IsOperator(input) )
                    {
                        /* case 1. operator is '='
                         * calculate the last formula "last operand + operator" of sub screen "+ current value" of main screen.
                         * update the result on main screen
                         * clear sub screen
                         * case 2. sub screen is empty
                         * copy main "text + operator" to sub screen
                         * keep main screen with first operand
                         * case 3. sub screen has text
                         * calculate the last formula "last operand + operator" of sub screen "+ current value" of main screen.
                         * update the result on main screen
                         */
                        if(_view.SubScreenText == "")
                        {
                            string s = "";

                            if (!ConvertKeyCodeToString(input, ref s))
                            {
                                return;
                            }
                            // 1. Find last operand & operator token from SubScreenText
                            // 2. calculate last operand
                            _view.SubScreenText = _view.ScreenText + s;
                            inputState = InputState.Operator;
                        }
                        else
                        {
                            string s = "";

                            if (!ConvertKeyCodeToString(input, ref s, operatorOnly:false))
                            {
                                return;
                            }
                            _view.SubScreenText += _view.ScreenText + s;

                            double result = 0;

                            // Calculate it!!
                            CalculateFormula(ref result);

                            if (input == KeyCode.Calculate)
                            {
                                _view.UpdateScreen(result, clearSubScreen: true);
                                inputState = InputState.Init;
                            }
                            else
                            {
                                _view.UpdateScreen(result, clearSubScreen: false);
                                inputState = InputState.Operator;
                            }
                        }
                    }
                    break;
                case InputState.OperandFractionalPart:
                    /* 1. point input
                     * break;
                     * 2. digit input
                     * s += input
                     * update main screen
                     * 3. operator input
                     * if sub string has perfect formula, 
                     * then calculate & update both screen (main & sub)
                     * else
                     * update sub screen += " operator "
                     * goto Operator state
                     */
                    if (IsPoint(input))
                    {
                        // Do nothing
                    }
                    else if (IsOperand(input))
                    {
                        if(input == KeyCode.Num0)
                        {
                            string s = _view.ScreenText;

                            // double has 15 decimal digits of precision
                            if (s.Length > 14)
                            {
                                return;
                            }

                            _view.UpdateScreen(s+"0");
                        }
                        else
                        {
                            AddDigitOnScreen(input);
                        }
                    }
                    else if (IsOperator(input))
                    {
                        if (_view.SubScreenText == "")
                        {
                            string s = "";

                            if (!ConvertKeyCodeToString(input, ref s))
                            {
                                return;
                            }
                            // 1. Find last operand & operator token from SubScreenText
                            // 2. calculate last operand
                            _view.SubScreenText = Convert.ToDouble(_view.ScreenText) + s;
                            inputState = InputState.Operator;
                        }
                        else
                        {
                            string s = "";

                            if (!ConvertKeyCodeToString(input, ref s, operatorOnly: false))
                            {
                                return;
                            }
                            _view.SubScreenText += _view.ScreenText + s;

                            double result = 0;

                            // Calculate it!!
                            CalculateFormula(ref result);

                            if (input == KeyCode.Calculate)
                            {
                                _view.UpdateScreen(result, clearSubScreen: true);
                                inputState = InputState.Init;
                            }
                            else
                            {
                                _view.UpdateScreen(result, clearSubScreen: false);
                                inputState = InputState.Operator;
                            }
                        }
                    }
                    break;
                case InputState.Operator:
                    /* 1. operator input
                     * if operator == "="
                     * calculat sub screen formula
                     * update main screen as result
                     * clear sub screen
                     * else
                     * replace previous operator
                     * 2. digit input
                     * s = digit input
                     * goto OperandIntPart state
                     */
                    if (IsOperand(input))
                    {
                        AddDigitOnScreen(input, true);
                        inputState = InputState.OperandIntegerPart;
                    }
                    else if (IsOperator(input))
                    {
                        if (input == KeyCode.Calculate)
                        {
                            double result = 0;

                            // Calculate it!!
                            CalculateFormula(ref result, allowGeneratedOperand: true);

                            _view.UpdateScreen(result, clearSubScreen: true);
                            inputState = InputState.Init;
                        }
                    }
                    break;
                default:
                    break;
            }

        }

        void DoBackSpace()
        {
            string s = _view.ScreenText;
            double d;

            if(!s.Contains('.'))
            {   // integer part
                if (s.Length <= 1)
                {
                    s = "0";
                    inputState = InputState.Init;
                }
                else
                {
                    inputState = InputState.OperandIntegerPart;

                    s = s.Substring(0, s.Length - 1);

                    RemoveComma(ref s);
                }

                double.TryParse(s, out d);

                _view.UpdateScreen(d);
            }
            else
            {   // fractional part
                if (s.Length <= 1)
                {
                    s = "0";
                    inputState = InputState.Init;
                }
                else
                {
                    inputState = InputState.OperandFractionalPart;

                    // if the last character of string is a dot
                    // then, state should change to integer part 
                    // because it's not a decimal number anymore.
                    if (s.Substring(s.Length - 1) == ".")
                    {
                        inputState = InputState.OperandIntegerPart;
                    }
                    // remove the last character of string
                    s = s.Substring(0, s.Length - 1);
                }

                _view.UpdateScreen(s);
            }
        }

        void TogglePlusMinus()
        {
            string s = _view.ScreenText;

            if (s.StartsWith("-"))
            {
                isNegative = false;
                _view.ScreenText = _view.ScreenText.TrimStart('-');
            }
            else
            {
                isNegative = true;
                if(s != "0")
                _view.ScreenText = _view.ScreenText.Insert(0, "-");
            }
        }

        bool ConvertKeyCodeToString(KeyCode input, ref string s, bool operatorOnly=true)
        {
            if (!operatorOnly)
            {
                if (input == KeyCode.Calculate)
                {
                    s = " = ";
                    return true;
                }
            }

            switch (input)
            {
                case KeyCode.Add:
                    s = " + ";
                    break;
                case KeyCode.Subtract:
                    s = " - ";
                    break;
                case KeyCode.Multiply:
                    s = " * ";
                    break;
                case KeyCode.Divide:
                    s = " / ";
                    break;
                default:
                    return false;
            }

            return true;
        }

        void CalculateFormula(ref double result, bool allowGeneratedOperand=false)
        {
            string subs = _view.SubScreenText;
            char[] del = { ' ' };
            string[] tokens = subs.Split(del);
            string history = subs;

            result = Convert.ToDouble(tokens[0]);

            for (int i = 1; i < tokens.Length - 1; i++)
            {
                if (i < tokens.Length - 2)
                {
                    switch (tokens[i])
                    {
                        case "+":
                            result = result + Convert.ToDouble(tokens[++i]);
                            break;
                        case "-":
                            result = result - Convert.ToDouble(tokens[++i]);
                            break;
                        case "*":
                            result = result * Convert.ToDouble(tokens[++i]);
                            break;
                        case "/":
                            result = result / Convert.ToDouble(tokens[++i]);
                            break;
                        default:
                            break;
                    }
                }
            }

            if(allowGeneratedOperand)
            {
                switch (tokens[tokens.Length - 2])
                {
                    case "+":
                        result = result + Convert.ToDouble(_view.ScreenText);
                        break;
                    case "-":
                        result = result - Convert.ToDouble(_view.ScreenText);
                        break;
                    case "*":
                        result = result * Convert.ToDouble(_view.ScreenText);
                        break;
                    case "/":
                        result = result / Convert.ToDouble(_view.ScreenText);
                        break;
                    default:
                        break;
                }
                history = subs + " " + _view.ScreenText + " = ";
            }

            history += result;
            _cal.historyNode.AddLast(history);
        }

        void RemoveComma(ref string s)
        {
            s = s.Replace(",", "");
            /*
            string pattern = @"[,]*";

            Regex reg = new Regex(pattern);
            s = reg.Replace(s, "");
            */
        }

        public void AddDigitOnScreen(KeyCode input, bool clearScreen=false)
        {
            double d;
            string s = "";
            //float emSize = 0f;

            if(clearScreen)
            {
                s += input - KeyCode.Num0;
            }
            else
            {
                s = _view.ScreenText;
                RemoveComma(ref s);

                // double has 15 decimal digits of precision
                if (s.Length > 14)
                {
                    return;
                }
                s += input - KeyCode.Num0;
            }

            double.TryParse(s, out d);

            _view.UpdateScreen(d);
        }

        public void OnHistory()
        {
            _cal.ToggleHistory();
            _view.ShowCalculator(!_cal.HistoryOn);
            _view.ShowHistory(_cal.HistoryOn);

            if(_cal.historyNode.Count == 0)
            {
                _view.HistoryText = "There\'s no history yet";
            }
            else
            {
                _view.HistoryText = "";

                foreach (string node in _cal.historyNode)
                {
                    _view.HistoryText += node + "\n";
                }
            }
        }

        public void ClearHistory()
        {
            int count = _cal.historyNode.Count;

            for (int i = 0; i < count; i++ )
            {
                _cal.historyNode.RemoveFirst();
            }
        }

        public void OnPower()
        {
            _cal.TogglePower();

            if(_cal.PowerOn)
            {
                _cal.HistoryOn = false;
                _view.ShowCalculator(true);
                _view.ShowHistory(false);
            }
            else
            {
                _view.ShowCalculator(true);
                _view.ShowHistory(false);
                ClearHistory();
            }
            _view.SetPower(_cal.PowerOn);
        }

        public void OnNavBar()
        {
            _cal.ToggleMenu();
            _view.ShowAbout(false);
            _view.ShowMenu(_cal.MenuOn);
        }

        public void OnAbout()
        {
            _cal.MenuOn = false;
            _view.ShowAbout(true);
        }
    }
}
