﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Model
{
    public class CalModel
    {
        public enum CalMode
        {
            Standard,
            Sientific,
            Programmer
        }

        private string version;
        private string copyright;
        private CalMode mode;

        private bool powerOn;
        private bool menuOn;
        private bool historyOn;

        public LinkedList<string> historyNode;

        public string Version
        {
            get { return version; }
            set { version = value; }
        }
        public string Copyright
        {
            get { return copyright; }
            set { copyright = value; }
        }
        public CalMode Mode
        {
            get { return mode; }
            set { mode = value; }
        }
        public bool PowerOn
        {
            get { return powerOn; }
            set { powerOn = value; }
        }
        public bool MenuOn
        {
            get { return menuOn; }
            set { menuOn = value; }
        }
        public bool HistoryOn
        {
            get { return historyOn; }
            set { historyOn = value; }
        }

        /*
        public struct FunctionFlagStruct
        {
            public bool PowerOn { get; set; }
            public bool MenuOn { get; set; }
            public bool HistoryOn { get; set; }
        }

        public FunctionFlagStruct funcFlag;
        */

        // Default Constructor
        public CalModel()
        {
            version = "Version 0.40b";
            copyright = "© 2018 Jaehyun Park. All rights reserved.";
            mode = CalMode.Standard;

            powerOn = false;
            menuOn = false;
            historyOn = false;

            historyNode = new LinkedList<string>();

            /*
            funcFlag = new FunctionFlagStruct();
            funcFlag.PowerOn = false;
            funcFlag.MenuOn = false;
            funcFlag.HistoryOn = false;
            */
        }

        public void TogglePower()
        {
            powerOn = !powerOn;
        }
        public void ToggleMenu()
        {
            menuOn = !menuOn;
        }
        public void ToggleHistory()
        {
            historyOn = !historyOn;
        }
    }
}
