﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

using Calculator.Model;
using Calculator.View;
using Calculator.Controller;

namespace SimpleCalculator
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            CalView view = new CalView();

            CalController controller = new CalController(view);

            controller.Initialize();

            Application.Run(view);

        }
    }
}
